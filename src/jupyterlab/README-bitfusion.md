# Bitfusion & GPU access

To provide shared access to a GPU farm, we assume that the Bitfusion Flexdirect client is installed
in both the container running the JupyterLab instances *and* on the servers running the Docker containers.

Flexdirect is a shim that forwards CUDA calls to a remote GPU farm for processing. For this to work the
NVidia drivers need to be in sync between the clients and the server(s) and the CUDA version also must be
compatible.

Currently the NVidia drivers can be installed like this:
````
     sudo apt-get install software-properties-common
     sudo apt-get install python3-software-properties
     sudo add-apt-repository ppa:graphics-drivers
     sudo apt-get update
     sudo apt-get install nvidia-430
````

The CUDA toolkit can be installed like this:
````
	sudo wget http://developer.download.nvidia.com/compute/cuda/repos/ubuntu1604/x86_64/cuda-repo-ubuntu1604_9.1.85-1_amd64.deb
	sudo dpkg -i cuda-repo-ubuntu1604_9.1.85-1_amd64.deb
	sudo apt-key adv --fetch-keys http://developer.download.nvidia.com/compute/cuda/repos/ubuntu1604/x86_64/7fa2af80.pub
	sudo apt-get update &&  apt-get install -y cuda-toolkit-9-1
	sudo rm cuda-repo-ubuntu1604_9.1.85-1_amd64.deb
    
````

Install these libraries and tools first, then install the Flexdirect client on the server where you
will run the Docker containers to verify that the server can talk to the GPU farm. You can do this with the
command 
````
     flexdirect smi
````
and the command
````
    flexdirect run -n 1 nvidia-smi
````

## Docker base image for Bitfusion Flexdirect

FLexdirect needs to run on an Ubumntu 16.04 base, and has an installer that is currently assumed to be
run interactively. Thus, we need to build a base image with appropriate libraries which we can then use 
as the base for the automated JupyterLab build. 

The Bitfusion base image source is found in the  bitfusion-docker-base-image subdirectory. After you build
that docker image, there are a couple manual steps to install Flexdirect and create a new Docker image

 1.) Run a bash shell inside a container running the image we build here
````
       sudo docker run --name bf-install -it ubuntu-bitfusion
````
 2.) From the bash shell, follow the Bitfusion directions to install 
 the flexdirect client

 3.) Exit the shell. There will be a dead container - find it with the command:
````
        sudo docker ps -a
````
 then commit the changes found in the dead container with this command:
 ````
        sudo docker commit -m="Ubuntu 16.04 with Flexdirect client" 5da4d4af1a72
````
 4.) Find the new untagged image with the command 
````
       sudo docker images
````
then tag the image using this command:
````
       sudo docker tag 1ac2779f1c66 ubuntu16.04-flexdirect
````




